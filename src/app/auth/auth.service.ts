import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthData } from './auth-data.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private token: string | null = null;
  private isAuthenticated = false;
  private authStatusListener = new Subject<boolean>();
  private userId: string | null = null;
  private tokenTimer?: any;

  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getUserId() {
    return this.userId;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  createUser(email: string, password: string) {
    const authData: AuthData = { email, password };
    return this.http
      .post(`${environment.apiUrl}/user/signup`, authData)
      .subscribe(
        () => {
          this.router.navigate(['/']);
        },
        () => {
          this.authStatusListener.next(false);
        }
      );
  }

  login(email: string, password: string) {
    const authData: AuthData = { email, password };

    this.http
      .post<{ token: string; expiresIn: number; userId: string }>(
        `${environment.apiUrl}/user/login`,
        authData
      )
      .subscribe(
        (response) => {
          this.token = response.token;
          if (this.token) {
            const expiresInDuration = response.expiresIn;
            this.setAuthTimer(expiresInDuration);
            this.authStatusListener.next(true);
            const dateNow = new Date();
            const expirationDate = new Date(
              dateNow.getTime() + expiresInDuration * 1000
            );
            this.isAuthenticated = true;
            this.userId = response.userId ?? null;
            this.saveAuthData(this.token, expirationDate, this.userId);
            this.router.navigate(['/']);
          }
        },
        () => {
          this.authStatusListener.next(false);
        }
      );
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    if (this.tokenTimer) {
      clearTimeout(this.tokenTimer);
    }
    this.clearAuthData();
    this.userId = null;
    this.router.navigate(['/']);
  }

  autoAuthUser() {
    const authInformatoin = this.getAuthData();
    if (!authInformatoin) {
      return;
    }
    this.userId = authInformatoin.userId;
    const dateNow = new Date();
    const expiresIn =
      authInformatoin?.expirationDate.getTime() - dateNow.getTime();
    if (expiresIn > 0) {
      this.token = authInformatoin.token;
      this.isAuthenticated = true;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
    }
  }

  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => this.logout(), duration * 1000);
  }

  private saveAuthData(token: string, expiresIn: Date, userId: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiresIn', expiresIn.toISOString());
    localStorage.setItem('userId', userId);
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiresIn');
    localStorage.removeItem('userId');
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiresIn');
    const userId = localStorage.getItem('userId');
    if (token && expirationDate) {
      return { token, expirationDate: new Date(expirationDate), userId };
    }
    return null;
  }
}
