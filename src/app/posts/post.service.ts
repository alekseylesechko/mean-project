import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Post } from './post.model';

type postsResponse = {
  message: string;
  posts: {
    _id: string;
    title: string;
    content: string;
    imagePath: string;
    creator?: string;
  }[];
  count?: number;
};

type postResponse = {
  message: string;
  post: {
    _id: string;
    title: string;
    content: string;
    imagePath: string;
    creator?: string;
  };
};

@Injectable({ providedIn: 'root' })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated: BehaviorSubject<{
    posts: Post[];
    totalPosts: number;
  }> = new BehaviorSubject({ posts: this.posts, totalPosts: 0 });

  constructor(private http: HttpClient, private router: Router) {}

  getPosts(postsPerPage: number = 0, currentPage: number = 1): void {
    const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}`;
    this.http
      .get<postsResponse>(`${environment.apiUrl}/posts` + queryParams)
      .pipe(
        map((res) => ({
          posts: res.posts.map((post) => ({
            id: post._id,
            title: post.title,
            content: post.content,
            imagePath: post.imagePath,
            creator: post.creator,
          })),
          totalPosts: res.count,
        }))
      )
      .subscribe((res) => {
        this.posts = res.posts;
        this.postsUpdated.next({
          posts: [...this.posts],
          totalPosts: res.totalPosts || 0,
        });
      });
  }

  getPostById(id: string): Observable<Post> {
    return this.http
      .get<postResponse>(`${environment.apiUrl}/posts/${id}`)
      .pipe(
        map((res) => ({
          id: res.post._id,
          title: res.post.title,
          content: res.post.content,
          imagePath: res.post.imagePath,
          creator : res.post.creator,
        }))
      );
  }

  addPost(post: Post, image: File): void {
    const postData = new FormData();
    postData.append('title', post.title);
    postData.append('content', post.content);
    postData.append('image', image, post.title);

    this.http
      .post(`${environment.apiUrl}/posts`, postData)
      .subscribe(() => {
        this.getPosts();
        this.router.navigate(['/']);
      });
  }

  updatePost(post: Post, image: File): void {
    let postData;
    if (image) {
      postData = new FormData();
      postData.append('title', post.title);
      postData.append('content', post.content);
      postData.append('image', image, post.title);
      postData.append('creator', post.creator!);
    } else {
      postData = post;
    }
    this.http
      .put(`${environment.apiUrl}/posts/${post.id}`, postData)
      .subscribe(() => {
        this.getPosts();
        this.router.navigate(['/']);
      });
  }

  onPostUpdated() {
    return this.postsUpdated.asObservable();
  }

  deletedPost(id: string): Observable<{ message: string }> {
    return this.http.delete<{ message: string }>(
      `${environment.apiUrl}/posts/${id}`
    );
  }
}
