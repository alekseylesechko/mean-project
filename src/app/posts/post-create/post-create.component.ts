import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { delay, finalize } from 'rxjs/operators';
import { Post } from '../post.model';
import { PostsService } from '../post.service';
import { mimeType } from './mime-type.validator';

@Component({
  selector: 'app-post-create',
  templateUrl: 'post-create.component.html',
  styleUrls: ['post-create-component.css'],
})
export class PostCreateComponent implements OnInit {
  form!: FormGroup;
  isLoading = false;
  imagePreview: string = '';
  private mode = 'create';
  private postId = '';

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.createForm();
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.mode = 'edit';
        this.postId = paramMap.get('id')!;
        this.isLoading = true;
        this.getPost();
      } else {
        this.mode = 'created';
      }
    });
  }

  createForm() {
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)],
      }),
      content: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)],
      }),
      imagePath: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType],
      }),
    });
  }

  getPost() {
    this.postsService
      .getPostById(this.postId)
      .pipe(
        delay(1000),
        finalize(() => (this.isLoading = false))
      )
      .subscribe((post) => {
        this.form.setValue({
          title: post.title,
          content: post.content,
          imagePath: post.imagePath,
        });
      });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files![0];
    this.form.patchValue({ imagePath: file });
    this.form.get('imagePath')?.updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  addImg(post: Post) {
    const img = this.form.value.imagePath;
    const newPost = { ...post };
    if (typeof img === 'string') {
      newPost.imagePath = img;
      return { newPost };
    }
    return { newPost, image: img };
  }

  onSavePost(): void {
    if (this.form.invalid) {
      return;
    }
    const post: Post = {
      title: this.form.value.title,
      content: this.form.value.content,
    };

    if (this.mode === 'edit') {
      post.id = this.postId;
      const { newPost, image } = this.addImg(post);
      this.postsService.updatePost(newPost as Post, image ?? null);
    } else {
      this.postsService.addPost(post, this.form.value.imagePath);
    }
  }
}
