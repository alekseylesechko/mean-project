import { Component, OnDestroy, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Post } from '../post.model';
import { PostsService } from '../post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
})
export class PostListComponent implements OnInit, OnDestroy {
  posts: Post[] = [];
  sub = new Subscription();
  isLoading = false;
  totalPosts = 10;
  postsPerPage = 2;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];
  userIsAuthenticated = false;
  userId: string | null = null;

  constructor(
    private postsService: PostsService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    let postSub = this.postsService.onPostUpdated().subscribe(
      ({ posts, totalPosts }) => {
        this.totalPosts = totalPosts;
        this.posts = posts;
        setTimeout(() => (this.isLoading = false), 1000);
      },
      () => (this.isLoading = false)
    );
    this.sub.add(postSub);
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.userId = this.authService.getUserId();
    this.authService.getAuthStatusListener().subscribe((isAuthenticated) => {
      this.userIsAuthenticated = isAuthenticated;
      this.userId = this.authService.getUserId();
    });

    this.getPosts();
  }

  onDelete(id: string | undefined) {
    if (id) {
      this.postsService.deletedPost(id).subscribe(() => {
        this.currentPage =
          this.posts.length - (1 % this.postsPerPage)
            ? this.currentPage
            : this.currentPage - 1 || 1;
        this.getPosts();
      });
    }
  }

  getPosts(): void {
    this.postsService.getPosts(this.postsPerPage, this.currentPage);
  }

  onChangePage(pageDate: PageEvent) {
    this.currentPage = pageDate.pageIndex + 1;
    this.postsPerPage = pageDate.pageSize;
    this.getPosts();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
