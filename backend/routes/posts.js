import express from "express";
import { checkAuth } from "../middleware/check-auth.js";
import { extractFile } from "../middleware/file.js";

import {
  getPosts,
  getPostById,
  createPost,
  changePost,
  deletePost,
} from "../controllers/post.js";

export const router = express.Router();

router.get("/", getPosts);

router.get("/:id", getPostById);

router.post("/", checkAuth, extractFile, createPost);

router.put("/:id", checkAuth, extractFile, changePost);

router.delete("/:id", checkAuth, deletePost);
