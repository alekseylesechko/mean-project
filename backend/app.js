import path from "path";
import express from "express";
import mongoose from "mongoose";
import { postsRouter, userRouter } from "./routes/index.js";

export const app = express();
app.use(express.json());
app.use("/images", express.static(path.join("backend/images")));

try {
  await mongoose.connect(
    `mongodb+srv://Alex:${process.env.MONGO_ATLAS_PW}@cluster0.tymox.mongodb.net/mean-course?retryWrites=true&w=majority`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    }
  );
} catch (e) {
  console.log(e);
}

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT,DELETE, OPTIONS"
  );

  next();
});

app.use("/api/posts", postsRouter);
app.use("/api/user", userRouter);
