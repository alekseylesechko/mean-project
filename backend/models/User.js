import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

const { Schema, model } = mongoose;

const user = Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
});

user.plugin(uniqueValidator);

export const User = model("User", user);
