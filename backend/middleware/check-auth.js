import jwt from "jsonwebtoken";

export function checkAuth(req, res, next) {
  try {
    const token = req.headers.authorization?.split(" ")[1];

    const {email, userId} = jwt.verify(token, process.env.JWT_KEY);
    req.userData = {email, userId}
    next();
  } catch(e) {
    res.status(401).json({ message: "You are not authenticated!" });
  }
}
