import express from "express";
import multer from "multer";
import { Post } from "../models/Post.js";

export const router = express.Router();

export async function getPosts(req, res) {
  const { pagesize: pageSize = 0, page: currentPage = 1 } = req.query;
  const options = {
    limit: +pageSize,
    skip: +pageSize * (+currentPage - 1),
  };

  try {
    const posts = await Post.find({}, { __v: 0 }, options);
    const count = await Post.count();

    res.json({
      message: "Post fetched successfully",
      posts,
      count,
    });
  } catch (e) {
    res.status(500).json({ message: "Fetching post failed" });
  }
}

export async function getPostById(req, res) {
  try {
    const post = await Post.findById(req.params.id);
    if (post) {
      return res.json({
        message: "Post fetched successfully",
        post,
      });
    }
    return res.status(400).json({
      message: "Post not found",
    });
  } catch (e) {
    res.status(500).json({ message: "Fetching post failed" });
  }
}

export async function createPost(req, res) {
  const url = `${req.protocol}://${req.get("host")}`;
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: `${url}/images/${req.file.filename}`,
    creator: req.userData.userId,
  });
  try {
    post.save();
    res.status(201).json({
      message: "Post created successfully",
    });
  } catch (e) {
    res.status(500).json({
      message: "Creating a post failed",
    });
  }
}

export async function changePost(req, res) {
  const id = req.params.id;
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = `${req.protocol}://${req.get("host")}`;
    imagePath = `${url}/images/${req.file.filename}`;
  }

  const post = {
    title: req.body.title,
    content: req.body.content,
    imagePath,
    creator: req.userData.userId,
  };
  try {
    let updatedPost = await Post.findOneAndUpdate(
      { _id: id, creator: req.userData.userId },
      post
    );
    if (updatedPost) {
      return res.json({ message: "Post updated successfully" });
    }
    res.status(401).json({ message: "Update error" });
  } catch (e) {
    res.status(401).json({ message: "Couldn`t update post" });
  }
}

export async function deletePost(req, res) {
  const id = req.params.id;

  try {
    let deletedPost = await Post.findOneAndDelete({
      _id: id,
      creator: req.userData.userId,
    });

    if (deletedPost) {
      return res.json({ message: "Post deleted successfully" });
    }
    res.status(401).json({ message: "Delete error" });
  } catch (e) {
    res.status(401).json({ message: "Deleting post failed" });
  }
}
