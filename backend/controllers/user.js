import bcrypt from "bcrypt";
import jws from "jsonwebtoken";
import { User } from "../models/User.js";

export async function createUser(req, res) {
  const password = await bcrypt.hash(req.body.password, 10);

  const user = new User({
    email: req.body.email,
    password,
  });

  try {
    await user.save();
    res.json({
      message: "User created",
    });
  } catch (err) {
    res
      .status(400)
      .json({ message: "Invalid authentication credentials!", error: err });
  }
}

export async function loginUser(req, res) {
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    return res
      .status(401)
      .json({ message: "Invalid authentication credentials!" });
  }

  const checkPassword = bcrypt.compare(req.body.password, user.password);
  if (checkPassword) {
    const token = jws.sign({ email: user.email, userId: user._id }, process.env.JWT_KEY, {
      expiresIn: "1h",
    });
    return res.status(200).json({ token, expiresIn: 3600, userId: user._id });
  }

  return res
    .status(401)
    .json({ message: "Invalid authentication credentials!", error: err });
}
